import { Component, OnInit, ElementRef, ViewChild, } from '@angular/core';
import { AsistenciaService } from 'src/services/asistencia.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AsistenciaService]
})

export class HomeComponent implements OnInit {

  @ViewChild('codigoEmpleado', { static: false }) codigoEmpleado: ElementRef;

  horas: any;
  minutos: any;
  segundos: any;
  codigo: string;
  loading: boolean = false;

  constructor(
    private _asistenciaService: AsistenciaService
  ) { }

  ngOnInit() {
    setInterval(() => {
      let day = new Date();
      this.horas = day.getHours();
      this.minutos = day.getMinutes();
      this.segundos = day.getSeconds();
    })

    setTimeout(() => {
      this.codigoEmpleado.nativeElement.focus();
    }, 1000);
  }

  registrarIngreso() {
    this.codigoEmpleado.nativeElement.blur();
    if (this.codigo == '') {
      return
    }
    this.loading = true;
    this.codigo = this.codigo.trim();
    this._asistenciaService.registrarEntrada(this.codigo).subscribe(
      (result: any) => {
        if (result.ok == false) {
          if (result.error == 406) {
            Swal.fire(
              `!Error!`,
              `No se encuentra al trabajador en la BD`,
              `error`,
            )
            this.codigo = '';
            this.loading = false;
          } else {
            Swal.fire(
              `!Error!`,
              `<b>${result.trabajador.NOMBRE_COMPLETO}</b>
                <br>Ya tiene un registro previo a las: ${result.trabajador.HORA_ENTRADA}`,
              `warning`
            )
            this.codigo = '';
            this.loading = false
          }
        } else {
          Swal.fire(
            `¡Correcto!`,
            `<b>${result.trabajador.NOMBRE_COMPLETO}</b> 
             <br> Se ha registrado correctamente su asistencia`,
            `success`
          )
          this.codigo = '';
          this.loading = false
        }
      }
    )
  }

  onBlur(e) {
    if (!this.loading)
      this.codigoEmpleado.nativeElement.focus();
  }

}
