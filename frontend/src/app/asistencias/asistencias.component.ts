import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/services/asistencia.service';

@Component({
  selector: 'app-asistencias',
  templateUrl: './asistencias.component.html',
  styleUrls: ['./asistencias.component.css'],
  providers: [AsistenciaService]
})
export class AsistenciasComponent implements OnInit {

  asistencias = [];

  constructor(
    private _asistenciaService: AsistenciaService
  ) { }

  ngOnInit() {
    this._asistenciaService.listarEntradas().subscribe(
      (result: any) => {
        this.asistencias = result.message;
        console.log(this.asistencias)
      }
    )
  }

}
