import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-reloj',
  templateUrl: './reloj.component.html',
  styleUrls: ['./reloj.component.css']
})
export class RelojComponent implements OnInit, OnDestroy {

  horas: any;
  minutos: any;
  segundos: any;

  timer: any;

  constructor() { }

  ngOnInit() {
    this.timer = setInterval(() => {
      let deg = 6;
      let hr = <HTMLBodyElement>document.querySelector("#hr")
      let mn = <HTMLBodyElement>document.querySelector("#mn")
      let sc = <HTMLBodyElement>document.querySelector("#sc")
      let day = new Date();
      let hh = day.getHours() * 30;
      let mm = day.getMinutes() * deg;
      let ss = day.getSeconds() * deg;
      hr.style.transform = `rotateZ(${hh + (mm / 12)}deg)`;
      mn.style.transform = `rotateZ(${mm}deg)`;
      sc.style.transform = `rotateZ(${ss}deg)`;

      this.horas = day.getHours();
      this.minutos = day.getMinutes();
      this.segundos = day.getSeconds();
    })
  }

  ngOnDestroy() {
    clearInterval(this.timer)
  }

}
