import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'asistencias', component: AsistenciasComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
