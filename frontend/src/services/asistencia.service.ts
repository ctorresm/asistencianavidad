import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class AsistenciaService {
    
    constructor(
        public http: HttpClient,
        public _router: Router
    ) { }

    registrarEntrada(codigo) {
        let horaJson = { codigo }
        let url = 'http://localhost:3000/api/registrarEntrada';
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(url, horaJson, { headers: headers });
    }

    listarEntradas() {
        let url = 'http://localhost:3000/api/listarEntradas';
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.get(url, { headers: headers });
    }

}
